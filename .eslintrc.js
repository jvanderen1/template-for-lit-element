module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "amd": true,
        "jest": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:lit/recommended"
    ],
    "parser": "@babel/eslint-parser",
    "plugins": [
        "lit"
    ]
};
