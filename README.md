# Template for lit-element

In an effort to outline how to better test Lit-Element Web Components, this example was created.

## Access

This example simply loads a single component with CSS and a profile picture. To be updated in the future.

## Usage
The following are required to run this project:

- [Node.js](https://nodejs.org/en/)

### Run the Application (Development Mode)
To run this application in development mode, it will need access to http://localhost:8080. With
`webpack-dev-server`, the application with start in development mode with a server, along with hot
reloading.

<details><summary><b>Show instructions</b></summary>

1. Install the required packages outlined in `package.json`:

    ```shell script
    $ npm install
    ```

2. Run the application in development mode:

    ```shell script
    $ npm run start
    ```

3. Go to http://localhost:8080 in your web browser.

</details>

### Build the Application (Production Mode)
To build this application in production mode. With
`webpack`, build a production bundle of JS/CSS with an HTML page.

<details><summary><b>Show instructions</b></summary>

1. Install the required packages outlined in `package.json`:

    ```shell script
    $ npm install
    ```

2. Build the application in production mode:

    ```shell script
    $ npm run build
    ```

3. Open the generated `index.html` page located under `/dist/index.html`.

</details>

### Run the Application Tests

#### Unit Tests

This project contains a unit test for the Lit-Element components. To be updated with more examples.

<details><summary><b>Show instructions</b></summary>

1. Install the required packages outlined in `package.json`:

    ```shell script
    $ npm install
    ```

2. Run the application's unit tests without a coverage report:

    ```shell script
    $ npm run test
    ```

   Or run the application's unit tests with a coverage report:

   ```shell script
   $ npm run test:coverage
   ```

</details>

#### Code Analysis Tests

The project also contains code analysis tests through linting.

<details><summary><b>Show instructions</b></summary>

1. Install the required packages outlined in `package.json`:

    ```shell script
    $ npm install
    ```

2. Run `eslint` on the application's source code:

    ```shell script
    $ npm run lint:src
    ```

   Run `eslint` with the `--fix` flag to potentially fix linting issues:

   ```shell script
   $ npm run lint:src:fix
   ```

</details>

## Acknowledgements

1. [Testing React Components](https://gitlab.com/jvanderen1/testing-react-components)
   by [Joshua Van Deren](https://gitlab.com/jvanderen1)
