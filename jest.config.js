module.exports = {
    "collectCoverageFrom": [
        "src/js/**/*.js"
    ],
    "transform": {
        "^.+\\.(j|t)s?$": "babel-jest"
    },
    "transformIgnorePatterns": [
        "node_modules/(?!(lit-element|lit-html|@open-wc|chai-a11y-axe)/)"
    ],
    "rootDir": "test/jest"
}
