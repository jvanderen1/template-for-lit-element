// Imports
// -------

// Decorators
import {customElement, property} from 'lit-element';
// Libraries
import {LitElement, html} from 'lit-element';
// Components
import './ProfileImage';

// Internal
// --------

const customElementName = 'profile-card';

@customElement(customElementName)
class ProfileCard extends LitElement {
    //
    // Properties

    @property({type: String, reflect: true}) imageSrc = null;
    @property({type: String, reflect: true}) name = "Name";
    @property({type: String, reflect: true}) title = "Title";

    //
    // Lifecycle Methods

    render() {
        return html`
            <link href="css/styles.css" rel="stylesheet" type="text/css"> 
            <div class="bg-white mx-auto max-w-sm shadow-lg rounded-lg overflow-hidden">
                <div class="sm:flex sm:items-center px-6 py-4">
                    ${ this.imageSrc && html`<profile-image src="${this.imageSrc}"></profile-image>` }
                    <div class="text-center sm:text-left sm:flex-grow">
                        <div class="mb-4">
                            <p class="text-xl leading-tight">${this.name}</p>
                            <p class="text-sm leading-tight text-grey-dark">${this.title}</p>
                        </div>
                        <div>
                            <button class="text-xs font-semibold rounded-full px-4 py-1 leading-normal bg-white border border-purple text-purple hover:bg-purple hover:text-white">Message</button>
                        </div>
                    </div>
                </div>
            </div>;
        `;
    }
}

// Exports
// -------

export {
    customElementName,
    ProfileCard
}

export default ProfileCard;
