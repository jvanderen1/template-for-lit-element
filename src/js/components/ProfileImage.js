// Imports
// -------

// Decorators
import {customElement, internalProperty, property} from 'lit-element';
// Libraries
import {LitElement, html} from 'lit-element';
import {ResizeObserver} from '@juggle/resize-observer';

// Internal
// --------

const customElementName = 'profile-image';
const placeHolderImages = {
    small: 'https://via.placeholder.com/64.png',
    large: 'https://via.placeholder.com/96.png'
}

@customElement(customElementName)
class ProfileCard extends LitElement {
    //
    // Properties

    // External
    @property({type: String, reflect: true}) src = null;
    // Internal
    @internalProperty() ro;
    @internalProperty() isDefaultSrc = true;

    //
    // Callback Methods

    resizeCallback() {
        const {
            innerWidth
        } = window;

        if (innerWidth >= 640) {
            this.src = 'large';
        } else {
            this.src = 'small';
        }
    }

    //
    // Lifecycle Methods

    firstUpdated(changedProperties) {
        super.firstUpdated(changedProperties);
        if (!this.src) {
            if (window.innerWidth >= 640) {
                this.src = 'large';
            } else {
                this.src = 'small';
            }
        } else {
            this.isDefaultSrc = false;
        }
    }

    connectedCallback() {
        super.connectedCallback();
        this.ro = new ResizeObserver(this.resizeCallback.bind(this));
        if (!this.src) {
            this.ro.observe(document.body);
        }
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        this.ro.unobserve(document.body);
    }

    render() {
        return html`
            <link href="css/styles.css" rel="stylesheet" type="text/css">
            <img class="block h-16 sm:h-24 rounded-full mx-auto mb-4 sm:mb-0 sm:mr-4 sm:ml-0" src="${this.isDefaultSrc ? placeHolderImages[this.src] : this.src}" alt=""/>
        `;
    }
}

// Exports
// -------

export {
    customElementName,
    ProfileCard
}

export default ProfileCard;
