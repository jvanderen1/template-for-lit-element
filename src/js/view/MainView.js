// Imports
// -------

// Decorators
import {customElement, property} from 'lit-element';
// Libraries
import {LitElement, html} from 'lit-element';
// Components
import '../components/ProfileCard';

// Internal
// --------

const customElementName = 'main-view';

@customElement(customElementName)
class MainView extends LitElement {
    //
    // Properties

    @property({type: String, reflect: true}) title = "default title";
    @property({type: String, reflect: true}) description = "default description";

    //
    // Lifecycle Methods

    render() {
        const imageSrc = encodeURI('https://avatars2.githubusercontent.com/u/4323180?s=400&u=4962a4441fae9fba5f0f86456c6c506a21ffca4f&v=4');
        return html`
            <profile-card imageSrc="${imageSrc}" name="Adam Wathan" title="Developer at NothingWorks Inc."></profile-card>;
        `;
    }
}

// Exports
// -------

export {
    customElementName,
    MainView
}

export default MainView;
