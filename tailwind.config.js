module.exports = {
    purge: {
        content: [
            './src/**/*.js',
            './src/**/*.html'
        ],
        enabled: true
    },
    theme: {
        extend: {}
    },
    variants: {},
    plugins: []
};
