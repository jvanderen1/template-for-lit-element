// Imports
// -------

// Components
import {customElementName} from "../../../src/js/view/MainApp";
// Utilities
import {getComponent} from "../../util/getComponent";

// Tests
// -----

describe('js/MainApp', () => {
  let component;
  let shadowRoot;

  describe('Rendering', () => {
    it('renders default correctly', async () => {
      component = await getComponent(customElementName);
      shadowRoot = component.shadowRoot?.innerHTML;
      expect(shadowRoot).toMatchSnapshot();
    });
  });
});
