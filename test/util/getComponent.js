// Imports
// -------

// Libraries
import {fixture} from "@open-wc/testing";

// Internal
// --------

async function getComponent(customElementName, props = {}) {
    const element = document.createElement(customElementName, props);
    return await fixture(
        element
    );
}

// Exports
// -------

export {getComponent}
